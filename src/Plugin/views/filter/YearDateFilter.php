<?php

namespace Drupal\views_month_select_filter\Plugin\views\filter;

/**
 * Filter plugin for a month select filter.
 *
 * @ViewsFilter("datetime_year_filter")
 */
class YearDateFilter extends DateFormulaFilterBase {

  /**
   * {@inheritdoc}
   */
  function getOptions() {
    $years = range($this->definition['start_year'], date('Y'));
    return array_combine($years, $years);
  }

  /**
   * {@inheritdoc}
   */
  function getDateFormatString() {
    return 'Y';
  }

}
