<?php

namespace Drupal\views_month_select_filter\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\views\filter\Date;

/**
 * A base class for date filters based on a formula.
 */
abstract class DateFormulaFilterBase extends Date {

  /**
   * The value for the empty option.
   */
  const EMPTY_OPTION = 'All';

  /**
   * Get the optiosn for the formula.
   *
   * @return array
   *   An array of options.
   */
  abstract function getOptions();

  /**
   * Get te date format string for the formula.
   *
   * @return string
   *   The date format string for the formula.
   */
  abstract function getDateFormatString();

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);

    // @todo, make the options work in the admin section.
    if (!$form_state->get('exposed')) {
      return;
    }

    $form['value']['#type'] = 'select';
    $form['value']['#size'] = 1;
    $form['value']['#options'] = $this->getOptions();
    $form['value']['#multiple'] = FALSE;
    $form['value']['#default_value'] = static::EMPTY_OPTION;
    $form['value']['#empty_value'] = static::EMPTY_OPTION;

    // Juggle user input in the same way the InOperator filter does. No idea why
    // this is necessary, but illegal value errors are raised otherwise.
    $exposed = $form_state->get('exposed');
    $identifier = $this->options['expose']['identifier'];
    $user_input = $form_state->getUserInput();
    if ($exposed && empty($user_input[$identifier])) {
      $user_input[$identifier] = static::EMPTY_OPTION;
      $form_state->setUserInput($user_input);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if ($this->value['value'] === static::EMPTY_OPTION) {
      return;
    }
    $this->ensureMyTable();

    $placeholder = $this->placeholder();
    $this->query->addWhere(0, $this->getFormula() . ' = ' . $placeholder, [
      $placeholder => $this->value['value'],
    ], 'formula');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormula() {
    return $this->getDateFormat($this->getDateFormatString());
  }

  /**
   * {@inheritdoc}
   */
  public function getDateField() {
    // Use string date storage/formatting since datetime fields are stored as
    // strings rather than UNIX timestamps.
    return $this->query->getDateField("$this->tableAlias.$this->realField", TRUE, $this->calculateOffset);
  }

}
